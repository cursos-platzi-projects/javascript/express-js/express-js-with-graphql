const courses = [
  {
    _id: "anyId",
    title: 'Mi titulo',
    teacher: 'Mi profesor',
    description: 'una descripcion',
    topic: 'programacion',
    people: [{
      _id: 'person1',
      name: 'Carlos',
      email: 'charly@gmail.com'
    }],
    level: 'intermedio'
  },
  {
    _id: "anyId2",
    title: 'Mi titulo 2',
    teacher: 'Mi profesor 2',
    description: 'una descripcion ',
    topic: 'programacion 2',
    people: [{
      _id: 'person1',
      name: 'Carlos',
      email: 'charly@gmail.com'
    }],
    level: 'avanzado'
  }
]

module.exports = {
  Query: {
    getCourses: () => {
      return courses
    },
    getCourse: (root, args) => {
      const course = courses.filter(course => course._id === args.id)
      return course.pop()
    }
  }
}

// module.exports = {
//   hello: () => {
//     return 'Hola mundo'
//   },
//   saludo: () => {
//     return 'Como estan todos'
//   }
// }