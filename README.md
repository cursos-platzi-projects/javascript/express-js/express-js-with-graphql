# Express JS - GraphQL

## Query normal

    getCourses {
      title
    }

## Query con parametro

    getCourse(id: "anyId") {
      title
      description
    }

## Alias

    AllCourses: getCourses {
      title
    }

    Course1: getCourse(id: "anyId") {
      title
      description
    }

## Fragmentos

    {
      AllCourses: getCourses {
        ...CourseFields
      }

      Course1: getCourse(id: "anyId") {
        ...CourseFields
        teacher
      }
  
      Course2: getCourse(id: "anyId2") {
        ...CourseFields
        topic
      }
    }

    fragment CourseFields on Course {
      _id
      title
      description
    }

## Variables

### Variable
    {
      "course": "anyId2"
    }

### Query usando la variable

    query GetCourse2 ($course: ID!) {
      getCourse(id: $course) {
        _id
        title
        people {
          _id
          name
        }
      }
    }

## Condicionales

### Variables

    {
      "monitor": false,
      "course": "anyId2"
    }

    query GetCourse2 ($monitor: Boolean!, $course: ID!) {
      getCourse(id: $course) {
        _id
        title
        ... on Course @include(if: $monitor) {
          description
        }
      }
    }